@extends('layouts.app')
@section('content')

<h1> Add a new car </h1>
<form method = 'post' action= "{{action('CarController@store')}}">
{{csrf_field()}}

<div class = "form-group">
<label for = "car" > What car would you like to add? </label>
<input type = "text" class ="form-control" name = "car">
</div>

<div class = "form-group">
<label for = "year" >  year </label>
<input type = "text" class ="form-control" name = "year">
</div>

<div class = "form-group">
<label for = "price" >  price </label>
<input type = "text" class ="form-control" name = "price">
</div>

<div class ="form-group">
<input type= "submit" class = "form-control" name= "submit" value = "save">
</div>
@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

</form>

@endsection