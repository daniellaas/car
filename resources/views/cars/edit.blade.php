@extends('layouts.app')
@section('content')

<h1> Edit car </h1>
<form method = 'post' action = "{{action('CarController@update', $car->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "car" >car to Update </label>
    <input type = "text" class ="form-control" name = "car" value= "{{$car->car}}">
</div>

<div class = "form-group">
    <label for = "year" >year car to Update </label>
    <input type = "text" class ="form-control" name = "year" value= "{{$car->year}}">
</div>

<div class = "form-group">
    <label for = "price" >price car to Update </label>
    <input type = "text" class ="form-control" name = "price" value= "{{$car->price}}">
</div>

<div class ="form-group">
    <input type= "submit" class = "form-control" name= "submit" value = "Update">
</div>

</form>


<form method = 'post' action = "{{action('CarController@destroy', $car->id)}}">
@csrf
@method('DELETE')


<div class ="form-group">
    <input type= "submit" class = "form-control" name= "submit" value = "Delete">
</div>
@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
</form>

@endsection