<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Car;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
        $cars = Car::all();
        return view('cars.index',['cars'=>$cars]);
    } 
    return redirect()->intended('/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
        if (Gate::denies('seller')) {
            abort(403,"Sorry you are not allowed to create cars..");
        }
        return view ('cars.create');
    } 
    return redirect()->intended('/home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
        if (Gate::denies('seller')) {
            abort(403,"Sorry you are not allowed to create cars..");
        }
        $this->validate($request,[
            'car'=>'required',
            'year'=>'required',
            'price'=>'required',
        ]);
        $cars=new Car();
        $id = Auth::id();
        $cars->car=$request->car;
        $cars->year=$request->year;
        $cars->price=$request->price;
        $cars->status=0;
        $cars->user_id=$id;
        $cars->save();

        return redirect('cars');
    } 
    return redirect()->intended('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
        if (Gate::denies('seller')) {
            abort(403,"Sorry you are not allowed to edit cars..");
        }
        $car= Car::find($id);
        return view ('cars.edit',compact('car'));
    } 
    return redirect()->intended('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
        if (Gate::denies('seller')) {
            abort(403,"Sorry you are not allowed to edit cars..");
        }
        $this->validate($request,[
            'car'=>'required',
            'year'=>'required',
            'price'=>'required',
        ]);
        $car = Car::find($id);
        $car->update($request->all());
        return redirect('cars');
    } 
    return redirect()->intended('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check()) {
        if (Gate::denies('seller')) {
            abort(403,"Sorry you are not allowed to delete cars..");
        }
 
        $car = Car::find($id);
        $car->delete();
        return redirect('cars');
        } 
        return redirect()->intended('/home');
    
    }

    public function buy($id)
    {
        if (Auth::check()) {
        if (Gate::denies('seller')) {
            abort(403,"Are you a hacker or what?");
        } 
        $car = Car::findOrFail($id); 
        $car->status = 1;
        $car->save();
        return redirect('cars');
        }
        return redirect()->intended('/home');
    }
}
