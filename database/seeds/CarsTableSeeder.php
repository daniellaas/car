<?php

use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert(
            [
                [
                'created_at' => date('Y-m-d G:i:s'),
                'car' => 'mazda6',
                'year' => 2016,
                'price' => 50000,
                'status'=> 0,
                'user_id' => 1,
                ],
                [
                'created_at' => date('Y-m-d G:i:s'),
                'car' => 'I10',
                'year' => 2014,
                'price' => 40000,
                'status'=> 0,
                'user_id' => 1,
                ],
                [
                'created_at' => date('Y-m-d G:i:s'),
                'car' => 'I25',
                'year' => 2017,
                'price' => 69000,
                'status'=> 0,
                'user_id' => 2,
                ],
            ]);

    }
}
