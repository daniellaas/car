<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                'name' => 'moshe',
                'email' => 'a@a.com ',
                'password' =>Hash::make ('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                'role' => 'seller',
                'credit' => 0,
                ],
                [
                'name' => 'dani',
                'email' => 'd@d.com ',
                'password' =>Hash::make ('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                'role' => 'seller',
                'credit' => 0,
                ],
                [
                'name' => 'orly',
                'email' => 'o@o.com ',
                'password' =>Hash::make ('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                'role' => 'byer',
                'credit' => 100000,
                ],
            ]);
    }
}
